#!/usr/bin/env python3.9

import os
import sys
import requests
import time
from datetime import datetime
from datetime import date
from datetime import timedelta
import csv


# Меню
def menu():
    print('''
--------------------------
| wildberries Bot v 2.85 |
--------------------------
''')


# Проверка есть ли дубликаты в списке (а именно файле orderlist.txt)
def check_duplicates(list_of_orders):
    if len(list_of_orders) == len(set(list_of_orders)):
        # Если дубликаты не найдены
        return False
    else:
        # Если дубликаты найдены
        return True


# Считыватель конфигурационного файла
def conf_csv():

    # Очищаем файл от комментариев которые начинаются со знака решётка "#"
    def decomment(csvfile):
        for row in csvfile:
            raw = row.split('#')[0].strip()
            if raw:
                yield raw

    # Считывание самих данных из файла
    with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), str('conf.csv')), 'r', encoding='utf-8') as conf_csv_file:
        conf_csv_data = csv.reader(decomment(conf_csv_file), delimiter=':', quotechar='"')
        conf_csv_result = {row[0]: row[1] for row in conf_csv_data}
        return conf_csv_result


# Запись лога в файл result.txt
# Немного не изящно, но так надо
def write_result(text):
    with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'result.txt'), "a", encoding='utf-8') as result:
        result.write(text + '\n')


# Запись лога в файл log.txt
# Немного не изящно, нужно будет поменять алгоритм
def write_log(text):
    with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'log.txt'),
              "a", encoding='utf-8') as log:
        log.write(text + '\n')


# Обработчик длины строки и что является числом
def str_length(f_string):
    if len(f_string) < 100:
        if f_string.isdigit():
            return True
        else:
            print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) +
                  ' Номер заказа должен быть в числовой форме. В файле orderlist.txt не должно быть пустых строк. ЗАВЕРШЕНИЕ РАБОТЫ!!!')
            write_log(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) +
                      ' Номер заказа должен быть в числовой форме. В файле orderlist.txt не должно быть пустых строк. ЗАВЕРШЕНИЕ РАБОТЫ!!!')
            sys.exit(0)
    else:
        print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' Введен слишком длинный номер заказа. ЗАВЕРШЕНИЕ РАБОТЫ!!!')
        write_log(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' Введен слишком длинный номер заказа. ЗАВЕРШЕНИЕ РАБОТЫ!!!')
        sys.exit(0)


def auth(user_login, user_passwd):
    try:
        session = requests.Session()
        url = 'https://suppliers.wildberries.ru/api/login'

        # Формируем http заголовки необходимые для запроса авторизации
        auth_headers = {
            'Host': 'suppliers.wildberries.ru',
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0',
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
            'Accept-Encoding': 'gzip, deflate, br',
            'Content-Type': 'application/json;charset=utf-8',
            'Content-Length': '86',
            'Origin': 'https://suppliers.wildberries.ru',
            'Connection': 'keep-alive',
            'Referer': 'https://suppliers.wildberries.ru/login',
        }

        # Формируем данные для авторизации
        auth_data = {
            "username": user_login,
            "password": user_passwd,
            "captcha_id": 'null',
            "captcha_solution": ""}

        if authorization := session.post(url, json=auth_data, headers=auth_headers):
            request_auth = authorization.headers
            return {'auth_success': request_auth, 'session': session}  # словарь со списком внутри
        else:
            print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) +' Ошибка авторизации. Неверный логин или пароль')
    except Exception:
        print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' Ошибка подключения к серверу')
        sys.exit(0)


# Получить список заказов
def get_preorders(session, resp_headers, orders_from_file):
    try:
        url = 'https://suppliers.wildberries.ru/supply-manager-api/api/v1/preorders'
        preorders_list = session.get(url, headers=resp_headers)

        # Если бэкенд сайта не вернул JSON с ключём Error: true, то продолжаем
        if not preorders_list.json()['error']:
            preorders_list_data = preorders_list.json()['data']['preOrders']

            preorder_list_allowed_for_supply = []  # список заказов разрешённых к поставке (информация с сайта)

            # заполняем список заказов разрешённых к поставке
            for data in preorders_list_data:
                if data['statusName'] == 'Разрешен к поставке':
                    preorder_list_allowed_for_supply.append(data['id'])

            order_list_filtered = []  # заказы из файла, проверенные, что они есть на сайте и разрешены к поставке
            # сравниваем есть ли в получившемся списке номера заказов из файла orderlist.txt
            for order in orders_from_file:
                order = order.strip()
                if str_length(order):
                    if int(order) in preorder_list_allowed_for_supply:
                        order_list_filtered.append(order)  # заполняем список заказов
                        print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' Заказ ' + str(order) + ' найден')
                    else:
                        print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' Заказ ' + str(order) + ' НЕ найден в списке разрешённых к поставке')

            order_list_summary = []  # Список с финальными полными данными по заказам
            # Берём проверенный список и дёргаем необходимые данные полученные с сайта
            for order in order_list_filtered:
                for data in preorders_list_data:
                    if data['statusName'] == 'Разрешен к поставке':
                        if data['id'] == int(order):
                            order_list_summary.append(data)

            return order_list_summary
        else:
            print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' Сервер вернул ошибку при получении списка заказов. ЗАВЕРШЕНИЕ РАБОТЫ!!!')
            sys.exit(0)
    except Exception:
        print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' Ошибка на стороне сервера при получении списка заказов. Сервер не отвечает. ЗАВЕРШЕНИЕ РАБОТЫ!!!')
        sys.exit(0)


# Получить доступные интервалы для доставки
def get_intervals(session, resp_headers, orders_data, def_date):
    try:
        intervals_summary = []  # результат скомпанновый в виде номер заяки - доступные интервалы
        for order in orders_data:
            url = 'https://suppliers.wildberries.ru/supply-manager-api/api/v1/intervals/available?boxTypeId=1&supplyDate=' \
                  + str(def_date) + '&warehouseId=' + str(order['warehouseId'])
            intervals_list = session.get(url, headers=resp_headers)
            if not intervals_list.json()['error']:
                intervals_summary.append({'order': order['id'], 'intervals': intervals_list.json()['data']['intervals']})
            else:
                print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) +
                      ' Сервер вернул ошибку при получении интервала доставки заказов. ЗАВЕРШЕНИЕ РАБОТЫ!!!')
                write_log(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) +
                      ' Сервер вернул ошибку при получении интервала доставки заказов. ЗАВЕРШЕНИЕ РАБОТЫ!!!')
                sys.exit(0)
        return intervals_summary
    except Exception:
        print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) +
              ' Ошибка на стороне сервера при получении списка интервалов доставки. Сервер не отвечает. ЗАВЕРШЕНИЕ РАБОТЫ!!!')
        write_log(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) +
                  ' Ошибка на стороне сервера при получении списка интервалов доставки. Сервер не отвечает. ЗАВЕРШЕНИЕ РАБОТЫ!!!')
        sys.exit(0)


# Создаём заявку на поставку (Планируем поставку)
def make_supply(session, resp_headers, orders_data, def_conf_data):
    controller = True
    interval_result = 'Без выбора интервала'  # Название интервала для записи в лог
    while controller:
        try:
            url = 'https://suppliers.wildberries.ru/supply-manager-api/api/v1/preorder'
            # Если в orders_data больше не осталось заказов без ключа order_controller
            if not any('order_controller' not in d for d in orders_data):
                print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' Заказы для оформления поставки закончились. ЗАВЕРШЕНИЕ РАБОТЫ!!!')
                controller = False
            else:
                # парсим заказы
                for order in orders_data:
                    if 'order_controller' not in order:
                        # убираем пометку разрыва исполнения цикла
                        break_loop = 0

                        # колличество дней на которое необходимо попытаться записать доставку
                        for i in range(0, int(def_conf_data['days'])):
                            if break_loop == 1:
                                break
                            else:
                                def_data = datetime.strptime(str(def_conf_data['date']), '%Y-%m-%d').date()
                                if def_data >= date.today():
                                    order_date = def_data + timedelta(days=i)  # дата подставляется в JSON
                                else:
                                    order_date = date.today() + timedelta(days=i)  # дата подставляется в JSON

                                # Если заказ идёт с выбором интервала парсим интервалы, чтобы вставить их в deliveryDate
                                # День, Ночь получаем интервалы доступные для доставки
                                intervals_data = get_intervals(auth_result['session'], resp_headers, orders_data, order_date)
                                for def_intervals in intervals_data:

                                    # Проверяем что номер заказа совпадает с номером заказа в интервалах, чтобы
                                    # состыковать интервалы заказа с нужным номером заказа
                                    if order['id'] == def_intervals['order']:
                                        if def_intervals['intervals']:

                                            #  считываем все списки интервалов доступные для данного заказа
                                            for intervals_id in def_intervals['intervals']:
                                                print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' Попытка записи заказа ' +
                                                      str(order['id']) + ' на ' + str(order_date) + ' ' + str(intervals_id['name']))

                                                # Контрольная сверка что номер заказа совпадает с номером заказа в
                                                # списке интервалов, а то мало ли что-то упустил
                                                if order['id'] == def_intervals['order']:
                                                    ordertime = datetime.now().strftime('%H:%M:%S')
                                                    json = {
                                                        "isDeleted": False,
                                                        "preOrderId": order['id'],
                                                        "deliveryDate": str(order_date) + "T" + ordertime,
                                                        "warehouseId": order['warehouseId'],
                                                        "monopalletCount": 0,
                                                        "intervalId": intervals_id['id']
                                                    }
                                                    # Название интервала для записи в лог
                                                    interval_result = intervals_id['name']
                                                    create_supply = session.post(url, json=json, headers=resp_headers)
                                                    if create_supply:
                                                        print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' Заказ ' + str(order['id']) +
                                                              ' успешно запланирован на поставку на ' + str(order_date) + ' ' + str(interval_result))

                                                        # Запись успеха в result.txt
                                                        write_result(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' Заказ ' + str(order['id']) +
                                                                     ' успешно запланирован на поставку на ' + str(order_date) + ' ' + str(interval_result))

                                                        # Добавляем в словарь в списоке на поставку которую удалось успешно запланировать ключ
                                                        # "order_controller": 1 чтобы программа не пыталась повторно оформить поставку

                                                        #orders_data.remove(next(item for item in orders_data if item["id"] == order['id']))
                                                        order.update({"order_controller": 1})

                                                        # Прерываем цикл, чтобы начать цикл заново иначе программа попытается выполнить
                                                        # заказ на день, даже если на ночь было успешно, так как цикл не закончился
                                                        break_loop = 1
                                                        break

                                                    elif create_supply.status_code == 400:
                                                        print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' ' + str(create_supply)
                                                              + ' ' + str(order['id']) + ' ' + str(create_supply.json()['errorText']))
                                                    else:
                                                        print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' ' + str(create_supply.json()))
                                                else:
                                                    write_log(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' КОНФЛИКТ номера заказа и интервалов')
                                                    print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' КОНФЛИКТ номера заказа и интервалов')
                                                    sys.exit(0)

                                        # Если возможности выбора интервала нет
                                        else:
                                            print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' Попытка записи заказа '
                                                  + str(order['id']) + ' на ' + str(order_date) + ' ' + str(interval_result))
                                            ordertime = datetime.now().strftime('%H:%M:%S')
                                            json = {
                                                "isDeleted": False,
                                                "preOrderId": order['id'],
                                                "deliveryDate": str(order_date) + "T" + ordertime,
                                                "warehouseId": order['warehouseId'],
                                                "monopalletCount": 0
                                            }
                                            # Название интервала для записи в лог
                                            interval_result = 'Без выбора интервала'
                                            create_supply = session.post(url, json=json, headers=resp_headers)
                                            if create_supply:
                                                print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' Заказ ' + str(order['id']) +
                                                      ' успешно запланирован на поставку на ' + str(order_date) + ' ' + str(interval_result))

                                                # Запись успеха в result.txt
                                                write_result('Заказ ' + str(order['id']) + ' успешно запланирован на поставку на '
                                                             + str(order_date) + ' ' + str(interval_result))

                                                # Добавляем в словарь в списоке на поставку которую удалось успешно запланировать ключ
                                                # "order_controller": 1 чтобы программа не пыталась повторно оформить поставку
                                                order.update({"order_controller": 1})

                                                # Прерываем цикл, и ставим пометку разрыва цикла уровнем выше
                                                # чтобы начать цикл заново иначе программа попытается выполнить заказ
                                                # на день, даже если на ночь было успешно, так как цикл не закончился
                                                break_loop = 1
                                                break

                                            elif create_supply.status_code == 400:
                                                print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' '
                                                      + str(create_supply) + ' ' + str(order['id']) + ' ' + str(create_supply.json()['errorText']))
                                            else:
                                                print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' '
                                                      + str(create_supply.json()))

        except Exception:
            print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) +
                  ' Ошибка на стороне сервера при отправке заявки на поставку. ПЕРЕЗАПУСК ПОСТАНОВКИ НА ДОСТАВКУ!!!')
            print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' Перезапуск через 10 секунд')
            time.sleep(10)
            write_log(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) +
                      ' Ошибка на стороне сервера при отправке заявки на поставку. ПЕРЕЗАПУСК ПОСТАНОВКИ НА ДОСТАВКУ!!!')


# Основная логика
if __name__ == '__main__':
    try:
        menu()
        # Считываем параметры из конфигурационного файла
        conf_data = conf_csv()
        print(conf_data)
        # Проверяем что есть ключ Set-Cookie. Он говорит о том, что авторизация выполнена и получены Куки
        auth_result = auth(conf_data['login'], conf_data['password'])
        if auth_result['auth_success']:
            print('Авторизация успешна. НАЧИНАЕМ РАБОТУ!!!\n')

            # пауза в 1 секунду чтобы успеть прочитать текст
            time.sleep(1)

            # Формируем http заголовки необходимые для работы на сайте
            headers = {
                'Host': 'suppliers.wildberries.ru',
                'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0',
                'Accept': '*/*',
                'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
                'Accept-Encoding': 'gzip, deflate, br',
                'Referer': 'https://suppliers.wildberries.ru/supply-manager',
                'Content-type': 'application/json',
                'Connection': 'keep-alive',
                'Cookie': str(auth_result['auth_success']['Set-Cookie'].split(';')[0])
            }

            # Считываем список заказов из файла orderlist.txt
            with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), "orderlist.txt"), "r", encoding='utf-8') as id_file:
                orders = id_file.readlines()

                # Очищаем все элементы списка от знака \n в конце строки
                orders = [s.rstrip() for s in orders]
            # Если файл orderlist.txt не пустой, то продолжаем
            if orders:
                # Если в файле orderlist.txt нет дубликатов, то продолжаем
                if not check_duplicates(orders):
                    preorders = get_preorders(auth_result['session'], headers, orders)

                    # Проверка остались ли в файле orderlist.txt заказы разрешённые к поставке
                    if preorders:
                        # делаем запись на поставку
                        make_supply(auth_result['session'], headers, preorders, conf_data)
                    else:
                        print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' В файле ordelist.txt не осталось заказов разрешённых к поставке. ЗАВЕРШЕНИЕ РАБОТЫ!!!')
                else:
                    print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' В файле orderlist.txt есть дубликаты заказов или пустые строки, удалите дубликаты и перезапустите программу. ЗАВЕРШЕНИЕ РАБОТЫ!!!')
            else:
                print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' Файл orderlist.txt пуст. Заполните его построчно номерами заказов. ЗАВЕРШЕНИЕ РАБОТЫ!!!')

        else:
            print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' Ошибка авторизации')
    except EOFError:
        print(str(datetime.strftime(datetime.now(), '%Y.%m.%d %H:%M:%S')) + ' Выход из приложения')
